/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationLogic.machine.machineStates;

/**
 *
 * @author Mateusz
 */
public enum Phase {     //phases are being used by FSM as group of states
    PHASE1, PHASE2, PHASE3, PHASE4;
}
