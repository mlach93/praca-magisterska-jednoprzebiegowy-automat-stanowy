/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationLogic.ipOperations;

/**
 *
 * @author Mateusz
 */
public class Converter {

    public static String convert(String ipDecimal) { 

        Integer[] splittedIPBytes = convertToInteger(splitBytes(ipDecimal));        //remove dots converts provided ip in string to integer and divedes each byte to table
        
        String convertedAddress = "";

        for (int i = 0; i < 4; i++) {
            convertedAddress += decimalToBinar(splittedIPBytes[i]);         //converts decimal ip decimal to binar version. Returns contatenated string String 
        }
        
        return convertedAddress;
        
    }

    private static String[] splitBytes(String addressIP) {

        String[] ipBytes = new String[4];

        int beginOfByte = 0;
        int endOfByte = addressIP.indexOf('.');

        for (int i = 0; i < 4; i++) {
            try {
                ipBytes[i] = addressIP.substring(beginOfByte, endOfByte);        // cuts byte after byte using beginOfByte and endOfByte

                beginOfByte = endOfByte + 1;
                endOfByte = addressIP.indexOf('.', beginOfByte);        // set new boundary for next byte

            } catch (IndexOutOfBoundsException e) {
                ipBytes[i] = addressIP.substring(beginOfByte);      //exception for last byte where there is no . on the end of byte
            }
            //System.out.println(ipBytes[i]); //uncoment to print in console

        }
        return ipBytes;
    }

    private static Integer[] convertToInteger(String[] ipBytesString) {

        Integer[] ipBytesInteger = new Integer[4];

        for (int i = 0; i < 4; i++) {
            try {
                ipBytesInteger[i] = Integer.valueOf(ipBytesString[i]);
            } catch (NumberFormatException e) {
                ipBytesInteger[i] = 0; // used if provided value is not a number
            }
        }

        return ipBytesInteger;
    }

    private static String decimalToBinar(Integer value) { 

        String binaryValue = Integer.toBinaryString(value);
        
        while(binaryValue.length() < 8) {
            binaryValue = 0 + binaryValue;      //adds insignificant zeros at the begining of byte
        }
            //System.out.println(binaryIPValue); //uncoment to print in console

        return binaryValue;
    }

}
