/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.actionListeners;

import applicationLogic.ipOperations.NetMask;
import applicationLogic.machine.ClassifierMachine;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import userInterface.ApplicationWindow;

/**
 *
 * @author Mateusz
 */
public class CIDRListener implements ChangeListener{

    private NetMask netMask;
    
    public CIDRListener(ApplicationWindow applicationWindow) {
        
        
        ClassifierMachine classifierMachine = applicationWindow.getClassifierMachine();
        this.netMask = classifierMachine.getNetMask();

    }
    
    @Override
    public void stateChanged(ChangeEvent ce) {       
        netMask.changeCidrState();
    }
    
    
    
}

