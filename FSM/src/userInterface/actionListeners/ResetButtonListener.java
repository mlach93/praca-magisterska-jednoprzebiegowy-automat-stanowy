/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.actionListeners;

import userInterface.panels.NetMaskPanel;
import userInterface.panels.MachineStatePanel;
import userInterface.panels.IPPanel;
import applicationLogic.machine.ClassifierMachine;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import userInterface.ApplicationWindow;

/**
 *
 * @author Mateusz
 */
public class ResetButtonListener implements ActionListener {

    private ClassifierMachine classifierMachine;

    private MachineStatePanel machineStatePanel;
    private NetMaskPanel netMaskPanel;
    private IPPanel ipConfirmPanel;
    private JPanel summaryPanel;

    public ResetButtonListener(ApplicationWindow applicationWindow) {
        this.classifierMachine = applicationWindow.getClassifierMachine();
        this.machineStatePanel = applicationWindow.getStatePanel();
        this.ipConfirmPanel = applicationWindow.getIpConfirmPanel();
        this.netMaskPanel = applicationWindow.getNetMaskPanel();
        this.summaryPanel = applicationWindow.getSummaryPanel();

    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        resetStatePanel();
        resetIpConfirmPanel();
        resetNetMaskPanel();
        resetSummaryPanel();
        resetClassifierMachine();

    }

    private void resetStatePanel() {
        machineStatePanel.getInput().setText("");
        machineStatePanel.getStack().setText("");
        machineStatePanel.getState().setText("");
        machineStatePanel.getStep().setText("");
    }

    private void resetIpConfirmPanel() {
        ipConfirmPanel.getCidrCheckBox().setEnabled(true);
        ipConfirmPanel.getCidrCheckBox().setSelected(false);
        ipConfirmPanel.getConfirmButton().setEnabled(true);
        ipConfirmPanel.getIpTextField().setEnabled(true);
        
    }

    private void resetNetMaskPanel() {
        netMaskPanel.setVisible(false);
        netMaskPanel.getConfirmButton().setEnabled(true);
        netMaskPanel.getNetMaskSlider().setEnabled(true);
        netMaskPanel.getNetMaskValue().setEnabled(true);
    }
    
    private void resetSummaryPanel() {
        summaryPanel.setVisible(false);
    }

    private void resetClassifierMachine() {
        classifierMachine.resetClassifierMachine();

    }

}
