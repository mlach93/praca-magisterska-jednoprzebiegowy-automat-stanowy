/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.actionListeners;

import userInterface.panels.NetMaskPanel;
import userInterface.panels.IPPanel;
import applicationLogic.ipOperations.*;
import applicationLogic.machine.ClassifierMachine;
import java.awt.event.*;
import static javax.swing.JOptionPane.*;
import javax.swing.JSlider;
import userInterface.ApplicationWindow;

/**
 *
 * @author Mateusz
 */
public class IPConfirmButtonListener implements ActionListener {

    private IPPanel ipConfirmPanel;
    private NetMaskPanel netMaskPanel;
    private AddressIP addressIP;
    private NetMask netMask;

    public IPConfirmButtonListener(ApplicationWindow applicationWindow) {

        ClassifierMachine classifierMachine = applicationWindow.getClassifierMachine();

        this.addressIP = classifierMachine.getAddressIP();
        this.netMask = classifierMachine.getNetMask();

        this.ipConfirmPanel = applicationWindow.getIpConfirmPanel();
        this.netMaskPanel = applicationWindow.getNetMaskPanel();

    }

    @Override
    public void actionPerformed(ActionEvent ae) {

//- Dodaj sprawdzanie czy adres jest klasy D lub E lub loopback przy metodzie CIDR jesli tak wyswietl komunikat ze podany adres jest nieobslugiwany
        boolean ipFormCorrect = false;
        String providedIP = ipConfirmPanel.getIpTextField().getText();
        providedIP = providedIP.trim();

        if (addressIP.checkIpCorrectness(providedIP)) {
            if (netMask.getCidrState()) {
                if (addressIP.checkCIDRIpCorrectness(providedIP)) {
                    ipFormCorrect = true;
                    if (ipFormCorrect) {
                        activateNetMaskPanel();
                    }
                }
            } else {
                ipFormCorrect = true;
            }

            if (ipFormCorrect) {
                addressIP.setAddressIP(providedIP);
                disableIPConfirmPanel();
            } else {
                System.err.println("Provided address does not matches CIDR criteria");
                showMessageDialog(null, "W przypadku wybrania metody CIDR dozwolony zakres adresów to adresy klas A-C z wyjątkiem adresu loopback.\nZakres: 1.0.0.0 - 223.255.255.255 z wyjątkiem 127.X.X.X");
            }

        } else {
            System.err.println("Provided address is not correct");
            showMessageDialog(null, "Zły format adresu, dozwolony zakres: 1.0.0.0. - 255.255.255.255");

        }

    }

    private void disableIPConfirmPanel() {
        ipConfirmPanel.getConfirmButton().setEnabled(false);        //disabling ipconfirm panel
        ipConfirmPanel.getCidrCheckBox().setEnabled(false);
        ipConfirmPanel.getIpTextField().setEnabled(false);
    }

    private void activateNetMaskPanel() {
        JSlider netMaskSlider = netMaskPanel.getNetMaskSlider();
        netMaskSlider.setMinimum(1);
        netMaskSlider.setValue(1);
        netMaskSlider.setMaximum(30);

        netMaskPanel.setVisible(true);
    }
}
