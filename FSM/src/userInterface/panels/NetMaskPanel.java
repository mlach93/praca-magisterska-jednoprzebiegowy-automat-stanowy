/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.panels;

import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;

/**
 *
 * @author Mateusz
 */
public class NetMaskPanel extends JPanel {

    private JSlider netMaskSlider;
    private JButton confirmButton;
    private JTextField netMaskValue;

    public NetMaskPanel() {

        this.netMaskSlider = new JSlider(0, 0, 0);
        this.confirmButton = new JButton("Potwierdź");
        this.netMaskValue = new JTextField();

        this.add(new JLabel("Maska: "));
        this.add(netMaskSlider);
        this.add(netMaskValue);
        this.add(confirmButton);
        
        configureTextField(netMaskValue);

    }

    public JSlider getNetMaskSlider() {
        return netMaskSlider;
    }

    public JButton getConfirmButton() {
        return confirmButton;
    }

    public JTextField getNetMaskValue() {
        return netMaskValue;
    }

    private void configureTextField(JTextField field) {
        field.setEditable(false);
        field.setPreferredSize(new Dimension(35, 30));
    }

}
