/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationLogic.ipOperations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Mateusz
 */
//stores Address IP in binary state
public class AddressIP {

    private String addressIP; //IP in binary form

    public String getAddressIP() {
        return addressIP;
    }

    public void setAddressIP(String addressIP) { //converts to binary form and removes dots
        this.addressIP = Converter.convert(addressIP) + "*";
        System.out.println("address IP " + addressIP);
    }

    public void ereaseAddressIP() {
        this.addressIP = null;
    }

    //
    public boolean checkIpCorrectness(String ipAddress) {

        Integer firstByte = Integer.valueOf(ipAddress.substring(0, ipAddress.indexOf(".")));

        if (firstByte == 0) {
            return false;
        } else {
            Pattern p = Pattern.compile("(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])");
            Matcher m = p.matcher(ipAddress);  // checks if provided by user input matches address IP pattern
            return m.matches();

        }
    }

    public boolean checkCIDRIpCorrectness(String ipAddress) {

        Integer firstByte = Integer.valueOf(ipAddress.substring(0, ipAddress.indexOf(".")));
        boolean ipCorrectnessState = !(firstByte > 223 || firstByte == 127);
        
        return ipCorrectnessState;
    }
}
