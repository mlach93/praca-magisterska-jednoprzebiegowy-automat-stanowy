/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationLogic.ipOperations;

import applicationLogic.machine.machineStates.State;

/**
 *
 * @author Mateusz
 */
public class NetMask {

    private static final String NET_MASK_A = "0000000000000000000000001111111";
    private static final String NET_MASK_B = "000000000000000011111111111111";
    private static final String NET_MASK_C = "00000000111111111111111111111";
    private static final String NET_MASK_D = "1111111111111111111111111111";
    private static final String NET_MASK_E = "1111111111111111111111111111";
    //reversed netMask Patterns used when machine recognizes addressIP class

    private String netMask;
    private Boolean cidrState;      //flag set when user choose cidr checkbox

    public NetMask() {
        initializeNetMask(); 
        this.cidrState = false;
    }

    public String getNetMask() {
        return netMask;
    }

    public Boolean getCidrState() {
        return cidrState;
    }

    public void changeCidrState() {
        cidrState = !cidrState;
    }

    private void initializeNetMask() {      //set basic pattern of net mask 32 X signs - later replaced by 1 and 0
        netMask = "";

        for (int i = 0; i < 32; i++) {
            this.netMask += 'X';
        }

    }

    public int returnNumberOfNetMaskBits() {
        int hostMaskLength = 0;
        int netMaskLength = 0;

        String maskToStack = netMask;
        for (Character c : maskToStack.toCharArray()) {
            if (c == '1') {
                netMaskLength = 32 - hostMaskLength;
                break;
            }
            hostMaskLength++;
        }

        return netMaskLength;
    }

    public void updateMask(int netMaskBits) {       //changes previously created pattern with X to reversed netMask

        int hostMaskBits = 32 - netMaskBits;
        String updatedMask = "";

        for (Character c : netMask.toCharArray()) {
            if (hostMaskBits-- > 0) {
                c = '0';
            } else {
                c = '1';
            }

            updatedMask += c;
        }
        netMask = updatedMask;
    }

    public void setMask(State state) {

        switch (state) {
            case SA:
                netMask = NET_MASK_A;
                break;
            case SB:
                netMask = NET_MASK_B;
                break;
            case SC:
                netMask = NET_MASK_C;
                break;
            case SD:
                netMask = NET_MASK_D;
                break;
            case SE:
                netMask = NET_MASK_E;
                break;
            default:
                netMask = "";
                break;
        }
    }

    public void clearMask() {
        initializeNetMask();
        cidrState = false;
    }

}
