/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationLogic.machine;

import java.util.Stack;

/**
 *
 * @author Mateusz
 */
public class MachineStack {

    private static final Character FIRST_STACK_ELEMENT = '*';

    private Stack<Character> stack;
    private Character topOfStack;

    public MachineStack() {
        this.stack = new Stack<Character>();
    }

    public Character getTopOfStack() {
        return topOfStack;
    }

    public void pushNext(Character c) {
        this.stack.push(c);
    }

    public void pushNext(String input) {    //overloaded pushNext metod to load more than one element to stack at once

        for (int i = 0; i < input.length(); i++) {
            pushNext(input.charAt(i));
        }
    }

    public void pushNext(Character c, String input) {       //overloaded pushNext metod to load more than one element to stack at once

        pushNext(c);
        for (int i = 0; i < input.length(); i++) {
            pushNext(input.charAt(i));
        }
    }

    public Character getPop() {
        Character pop = this.stack.pop();
        topOfStack = pop;

        return pop;
    }

    public void resetStack() {
        stack.clear();
        topOfStack = FIRST_STACK_ELEMENT;
        stack.push(FIRST_STACK_ELEMENT);
    }
    
    public void clearStack() {
        stack.clear();
    }

    public boolean empty() {
        return stack.empty();
    }

    //to delete
    @Override
    public String toString() {
        return this.stack.toString(); //To change body of generated methods, choose Tools | Templates.
    }

}
