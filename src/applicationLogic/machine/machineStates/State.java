/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationLogic.machine.machineStates;

/**
 *
 * @author Mateusz
 */
public enum State {

    S1(1), S2(2), S3(3), S4(4), // Basic, transitional states to recognizes IP class
    SA(100), SB(200), SC(300), SD(400), SE(500), SZ(600), SL(700), //Class state recognizes IP A-E class states, Z - CIDR method, LOOPBACK METHOD 
    SAN(110), SAB(120), SAH(130), //XXN - network address, host part equals 0; 
    SBN(210), SBB(220), SBH(230), //XXB - broadcast address host part equals 1; 
    SCN(310), SCB(320), SCH(330), //XXH - host address
    SZN(610), SZB(620), SZH(630);

    private int stateValue;

    public static final int NET_ADDRESS_ID = 10;
    public static final int BROADCAST_ADDRESS_ID = 20;
    public static final int HOST_ADDRESS_ID = 30;
    public static final int LOOPBACK_ADDRESS_ID = 700;
    public static final int CLASS_STATE_MULTIPLIER = 100;
    public static final int BROADCAST_CHECKSUM = 0;
    public static final int NET_CHECKSUM = 10;

    private State(int value) {
        this.stateValue = value;
    }

    public int getStateValue() {
        return this.stateValue;
    }
}
