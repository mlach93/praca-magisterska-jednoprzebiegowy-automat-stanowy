/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationLogic.machine.helpers;

/**
 *
 * @author Mateusz
 */
public class MachineMemory {

    private String memory;
    private String lastInput;
    private String lastStack;
    private String lastStep;
    private String lastState;

    public MachineMemory() {
        memory = "";
    }

    public String getMemory() {
        return memory;
    }

    public String getLastInput() {
        return lastInput;
    }

    public String getLastStack() {
        return lastStack;
    }

    public String getLastStep() {
        return lastStep;
    }

    public String getLastState() {
        return lastState;
    }

    public void addRecord(String record) {
        memory += record;
        lastStep = record.substring(record.indexOf(": ") + 1, record.indexOf(" |"));
        lastStep = lastStep.trim();
        record = record.substring(record.indexOf(" |") + 1);

        lastState = record.substring(record.indexOf(": ") + 1, record.indexOf(" |"));
        lastState = lastState.trim();
        record = record.substring(record.indexOf(" |") + 1);

        lastInput = record.substring(record.indexOf(": ") + 1, record.indexOf(" |"));
        lastInput = lastInput.trim();
        record = record.substring(record.indexOf(" |") + 1);

        lastStack = record.substring(record.indexOf(": "));
        lastStack = lastStack.trim();
        lastStack = record.substring(record.indexOf(" |") + 1);

    }
    

}
