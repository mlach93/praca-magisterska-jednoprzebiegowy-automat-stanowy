/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationLogic.machine.helpers;

import applicationLogic.machine.MachineStack;
import applicationLogic.machine.machineStates.State;

/**
 *
 * @author Mateusz
 */
public class Printer {

    private static final String DEFUALT_CLASS_TEXT_1 = "Wprowadzony adres IP należy do klasy ";
    private static final String DEFUALT_CLASS_TEXT_2 = ", jest on adresem ";
    private static final String CLASS_D_TEXT = "Wprowadzony adres IP należy do klasy D, adresy z tej klasy służą do wysyłania wiadomości grupowych.";
    private static final String CLASS_E_TEXT = "Wprowadzony adres IP należy do klasy E, adresy z tej klasy zarezerwowane są do celów badawczych.";
    private static final String CLASS_L_TEXT = "Wprowadzony adres IP jest adresem Loopback wykorzystywanym do połączeń testowych.";
    private static final String CLASS_Z_TEXT = "Wprowadzony adres IP wykorzystuje metodę CIDR ";

    public static String printMachineStatus(int step, char nextInput, State state, Character stackInput) {

        String stepField = customizeTextField("Krok: " + (step + 1), 7);
        String stateField = customizeTextField("  |  Bieżący stan: " + state, 3);

        String output = stepField + stateField + "   |   Wejście: " + nextInput + "   |   Szczyt stosu: " + stackInput + "      \n";
        System.out.print(output);
        return output;
    }

    private static String customizeTextField(String field, int minimumLength) {
        if (field.length() == minimumLength) {
            field += "  ";
        }
        return field;
    }

    public static String printStateSummary(String state, String addressIP) {
        String output = "";
        Character classPart = ' ';
        Character hostPart = ' ';
        try {
            classPart = state.charAt(1);
            hostPart = state.charAt(2);
        } catch (Exception e) {
            //e.printStackTrace(System.out);

        }

        switch (classPart) {
            case 'D':
                output += CLASS_D_TEXT;
                break;
            case 'E':
                output += CLASS_E_TEXT;
                break;
            case 'L':
                output += CLASS_L_TEXT;
                break;
            default:
                if (classPart == 'Z') {
                    output += CLASS_Z_TEXT;
                } else {
                    output += DEFUALT_CLASS_TEXT_1 + classPart;
                }
                output += DEFUALT_CLASS_TEXT_2;

                switch (hostPart) {
                    case 'N':
                        output += "sieci";
                        break;
                    case 'B':
                        output += "rozgłoszeniowym";
                        break;
                    default:
                        output += "hosta";
                        break;
                }
                break;
        }

        if (classPart != 'E' && classPart != 'D' && classPart != 'L') {
            output += recognizeAddressType(addressIP);
        }
        return output;
    }

    private static String recognizeAddressType(String addressIP) {
        String output = "";

        String firstByte = addressIP.substring(0, 8);
        String bSecondByte = addressIP.substring(8, 12);
        String cSecondByte = addressIP.substring(8, 16);

        final String aClassPrivate = "00001010";
        final String bClassPrivate1 = "10101100";
        final String bClassPrivate2 = "0001";
        final String cClassPrivate1 = "11000000";
        final String cClassPrivate2 = "10101000";

        if (firstByte.equals(aClassPrivate) || (firstByte.equals(bClassPrivate1) && bSecondByte.equals(bClassPrivate2)) || firstByte.equals(cClassPrivate1) && cSecondByte.equals(cClassPrivate2)) {
            output += " i należy do puli adresów prywatnych.";
        } else {
            output += " i należy do puli adresów publicznych.";
        }

        return output;

    }

    public static void printStack(MachineStack stack) {
        System.out.println(" stack " + stack + " <--");
    }

    public static void printLine() {
        System.out.println("__________________________________________________");
    }
}
