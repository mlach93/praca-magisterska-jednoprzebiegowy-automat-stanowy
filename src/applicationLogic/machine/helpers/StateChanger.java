/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationLogic.machine.helpers;

import applicationLogic.machine.machineStates.Phase;
import applicationLogic.machine.machineStates.State;

/**
 *
 * @author Mateusz
 */
public class StateChanger {

    public static Phase checkCurrentPhase(State state) { //this FSM method is checking on what level of recognizing IP address machine is

        int currentStateValue = state.getStateValue();

        if (currentStateValue < 10) {       //if value < 10 Phase1 FSM is recognizing trazitional state
            return Phase.PHASE1;
        } else if (currentStateValue % 100 == 0) {     //if value is divisible by 100 like 100, 200 etc. Phase2 class has been recognized
            return Phase.PHASE2;
        } else {      //if value >100 but not divisible by 100 Phase3 recognized address as host, net, or broadcast address
            return Phase.PHASE3;
        } 

    }

    public static State setNewState(int newStateValue, State state) {       
        for (State s : State.values()) {        //browses all posible states and chooses state which is corresponding to newStateValue
            if (s.getStateValue() == newStateValue) {
                state = s; 
            }
        }
        return state;
    }

}
