/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationLogic.machine;

import applicationLogic.machine.machineStates.Phase;
import applicationLogic.machine.machineStates.State;
import applicationLogic.machine.helpers.Printer;
import applicationLogic.ipOperations.AddressIP;
import applicationLogic.ipOperations.NetMask;
import applicationLogic.machine.helpers.MachineMemory;
import applicationLogic.machine.helpers.StateChanger;
import static applicationLogic.machine.helpers.StateChanger.*;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import userInterface.ApplicationWindow;
import userInterface.actionListeners.AutomaticStartButtonListener;
import userInterface.panels.NetMaskPanel;
import userInterface.panels.SummaryPanel;

/**
 *
 * @author Mateusz
 */
public class ClassifierMachine {

    private int step;
    private ApplicationWindow applicationWindow;

    private AddressIP input;
    private State state;
    private MachineStack stack;

    private NetMask netMask;
    private String addressIP;
    private MachineMemory machineMemory;

    public ClassifierMachine(AddressIP ipBinary, ApplicationWindow applicationWindow) {
        this.applicationWindow = applicationWindow;
        this.input = ipBinary;
        this.step = 0;
        this.state = State.S1;
        this.machineMemory = new MachineMemory();
        this.netMask = new NetMask();
        this.stack = new MachineStack();
    }

    public String getStep() {
        return Integer.toString(step + 1);
    }

    public String getInput() {
        char topOfInput = input.getAddressIP().charAt(step);
        return Character.toString(topOfInput);
    }

    public String getState() {
        return state.toString();
    }

    public String getTopOfStack() {
        return Character.toString(stack.getTopOfStack());
    }

    public MachineStack getStack() {
        return stack;
    }

    public NetMask getNetMask() {
        return netMask;
    }

    public MachineMemory getMachineMemory() {
        return machineMemory;
    }

    public AddressIP getAddressIP() {
        return input;
    }

    private void setStartingValues() {
        Printer.printLine();
        this.machineMemory = new MachineMemory();
        addressIP = input.getAddressIP();

        if (netMask.getCidrState()) { //if CIDR was not selected, reset stack and put '*' sign of top of it
            stack.resetStack();
        }
    }

    public void stepByStepStart() throws NullPointerException, StringIndexOutOfBoundsException {        //method called after clicking "Dalej" button on UI
        if (step == 0) {
            setStartingValues();
        }

        if (step < addressIP.length()) {
            doMachineNextStep();
            step++;
        }
    }

    public void automaticStart(AutomaticStartButtonListener automaticStartButtonListener) throws NullPointerException, StringIndexOutOfBoundsException {        //method chosen after clicking "Dalej automatycznie on UI"
        if (step == 0) {
            setStartingValues();
        }

        while (step < addressIP.length()) {         //in this loop each of 32 bits is being read and forwarded to FSM
            doMachineNextStep();
            automaticStartButtonListener.presentInWindow();     //updates view of performed work in UI
            step++;
        }
    }

    private void doMachineNextStep() {       //this FSM method takes nextInput, top of the stack and it checks current  state

        Character nextInput = addressIP.charAt(step);
        Character topOfStack = stack.getPop();
        Phase currentPhase = StateChanger.checkCurrentPhase(state);      //checks on what phase is FSM currently, what are possible ways to go? /available phases: tranistional state/ class reognizing/host,broadcast,net address

        machineMemory.addRecord(Printer.printMachineStatus(step, nextInput, state, topOfStack));        //add current step to memory
        changeMachineState(currentPhase, nextInput, topOfStack);

        if (nextInput == '*') {     //if machine ended up work show summaryPanel in UI 
            activateSummaryPanel();
        }

        //System.out.println("Address IP: " + addressIP); //uncomment to see invistigated IP
        //Printer.printStack(stack);// uncomment to see stack
    }

    private void changeMachineState(Phase phase, char nextInput, char topOfStack) {
        int newStateValue = 0;

        switch (phase) {
            case PHASE1:
                newStateValue = runPhase1(nextInput, topOfStack);       //recognizing class
                break;

            case PHASE2:
                newStateValue = runPhase2(nextInput, topOfStack);       //recognizing hostpart
                break;

            case PHASE3:
                newStateValue = runPhase3(nextInput, topOfStack);       //recogninizg net / host / broadcast address
                break;
        }

        state = setNewState(newStateValue, state);       //updating state
    }

    private int runPhase1(char nextInput, char topOfStack) {
        int stateValue = state.getStateValue();

        if (nextInput == '1' && topOfStack == '*') {        //moves to next transitional basic state
            stateValue++;

            if (stateValue < 5) {
                stack.pushNext(topOfStack);
            } else { //class E recognized net mask is being loaded
                stateValue *= State.CLASS_STATE_MULTIPLIER;
                createIPMask(stateValue);
                stack.pushNext(topOfStack, netMask.getNetMask());
            }

        } else if (nextInput == '0' && topOfStack == '*') {      //moves to class state
            stateValue *= State.CLASS_STATE_MULTIPLIER;
            createIPMask(stateValue);
            if (stateValue == State.SA.getStateValue()) {
                stateValue = State.LOOPBACK_ADDRESS_ID;
            }
            if (stateValue < 400) {
                activateNetMaskPanel(); //activates net Mask Panel for classes A-C to create subnet mask
            } else {
                stack.pushNext(topOfStack, netMask.getNetMask());
            }

        } else if ((nextInput == '0' || nextInput == '1') && topOfStack == '1') {       //case if CIDR method has been chosen in UI
            stateValue = State.SZ.getStateValue(); //Set states Value as CIDR SZ = CIDR
        }

        return stateValue;
    }

    private void createIPMask(int stateValue) {
        state = StateChanger.setNewState(stateValue, state);     // need to set new state here to create IP mask
        netMask.setMask(state);
    }

    private int runPhase2(char nextInput, char topOfStack) {        //method skips all operations until '1' from stack are removed
        int stateValue = state.getStateValue();
        if (stateValue == State.LOOPBACK_ADDRESS_ID) {
            if (topOfStack == '1') {
                if (nextInput == '0') {
                    stateValue = State.SA.getStateValue();
                }
            }
        } else if (topOfStack == '0') {
            if (nextInput == '0') {          //recognizes net address
                stateValue += State.NET_ADDRESS_ID; //10
            } else {        //recognizes broadcast address
                stateValue += State.BROADCAST_ADDRESS_ID; //20
            }
        }

        return stateValue;
    }

    private int runPhase3(char nextInput, char topOfStack) {
        int stateValue = state.getStateValue();

        int stateCheckSum = stateValue % 20;       //if statevalue divisionable by 20 means broadcast address

        if (topOfStack == '0') {
            if ((nextInput == '0') && (stateCheckSum == State.BROADCAST_CHECKSUM)) {
                stateValue = stateValue + State.HOST_ADDRESS_ID - State.BROADCAST_ADDRESS_ID;
            } else if ((nextInput == '1') && (stateCheckSum == State.NET_CHECKSUM)) {
                stateValue = stateValue + State.HOST_ADDRESS_ID - State.NET_ADDRESS_ID;
            }
        }

        return stateValue;
    }

    private void activateNetMaskPanel() {

        NetMaskPanel netMaskPanel = applicationWindow.getNetMaskPanel();
        JSlider netMaskSlider = netMaskPanel.getNetMaskSlider();

        int netMaskLength = netMask.returnNumberOfNetMaskBits();

        netMaskSlider.setMinimum(netMaskLength);
        netMaskSlider.setValue(netMaskLength);
        netMaskSlider.setMaximum(30);
        netMaskPanel.setVisible(true);

    }

    private void activateSummaryPanel() {
        SummaryPanel summaryPanel = applicationWindow.getSummaryPanel();
        JTextArea summaryText = summaryPanel.getSummaryText();

        summaryText.setText(Printer.printStateSummary(getState(), addressIP));

        summaryPanel.setVisible(true);
    }

    public void resetClassifierMachine() {
        setStartingValues(); //po co?
        step = 0;
        state = State.S1;
        input.ereaseAddressIP();
        stack.clearStack();
        netMask.clearMask();

    }

}
