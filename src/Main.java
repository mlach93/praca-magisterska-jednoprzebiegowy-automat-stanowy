/**
 *
 * @author Mateusz
 */

import javax.swing.SwingUtilities;
import userInterface.ApplicationWindow;

public class Main {

    public static void main(String[] args) {        
        SwingUtilities.invokeLater(new ApplicationWindow());
        
    }
    
}
