/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.panels;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author Mateusz
 */
public class ControlPanel extends JPanel {

    private JButton allStepsButton;
    private JButton nextStepButton;
    private JButton resetButton;
    private JButton showHistoryButton;
    private JButton diagramButton;

    public ControlPanel() {
        this.allStepsButton = new JButton("Dalej automatycznie");
        this.nextStepButton = new JButton("Dalej");
        this.resetButton = new JButton("Resetuj");
        this.showHistoryButton = new JButton("Wyświetl historię");
        this.diagramButton = new JButton("Wyświetl graf");

        this.add(nextStepButton);
        this.add(allStepsButton);
        this.add(resetButton);
        this.add(showHistoryButton);
        this.add(diagramButton);
        

    }

    public JButton getAllStepsButton() {
        return allStepsButton;
    }

    public JButton getNextStepButton() {
        return nextStepButton;
    }

    public JButton getResetButton() {
        return resetButton;
    }

    public JButton getShowHistoryButton() {
        return showHistoryButton;
    }

    public JButton getDiagramButton() {
        return diagramButton;
    }

    
    
}
