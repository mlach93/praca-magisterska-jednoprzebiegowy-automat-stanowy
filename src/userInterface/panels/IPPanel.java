/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.panels;

import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Mateusz
 */
public class IPPanel extends JPanel {

    private final String FIXED_IP = "134.1.25.2";

    private JTextField ipTextField;
    private JButton confirmButton;
    private JCheckBox cidrCheckBox;

    public IPPanel() {
        this.ipTextField = new JTextField(FIXED_IP);
        this.confirmButton = new JButton("Potwierdź");
        this.cidrCheckBox = new JCheckBox("CIDR");

        ipTextField.setPreferredSize(new Dimension(150, 40));

        this.add(new JLabel("Adres IP: "));
        this.add(ipTextField);
        this.add(cidrCheckBox);
        this.add(confirmButton);
    }

    public JTextField getIpTextField() {
        return ipTextField;
    }

    public JButton getConfirmButton() {
        return confirmButton;
    }

    public JCheckBox getCidrCheckBox() {
        return cidrCheckBox;
    }

}
