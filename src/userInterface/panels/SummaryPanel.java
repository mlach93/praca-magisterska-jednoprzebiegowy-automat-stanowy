/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.panels;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author Mateusz
 */
public class SummaryPanel extends JPanel {

    private JTextArea summaryText;
    
    public SummaryPanel() {
        
        this.summaryText = new JTextArea("");
        
        this.add(new JLabel("Podsumowanie:"));
        this.add(summaryText);
        
        this.setVisible(false);
    }

    public JTextArea getSummaryText() {
        return summaryText;
    }

    
    
}
