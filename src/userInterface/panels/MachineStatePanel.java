/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.panels;

import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Mateusz
 */
public class MachineStatePanel extends JPanel {

    private JTextField step;
    private JTextField state;
    private JTextField stack;
    private JTextField input;

    public MachineStatePanel() {

        this.step = new JTextField();
        this.state = new JTextField();
        this.stack = new JTextField();
        this.input = new JTextField();

        this.add(new JLabel("Krok: "));
        this.add(step);
        this.add(new JLabel(" Stan: "));
        this.add(state);
        this.add(new JLabel(" Wejście: "));
        this.add(input);
        this.add(new JLabel(" Stos: "));
        this.add(stack);
        
        
        configureTextField(step);
        configureTextField(state);
        configureTextField(stack);
        configureTextField(input);
       
    }

    public JTextField getStep() {
        return step;
    }

    public JTextField getState() {
        return state;
    }

    public JTextField getStack() {
        return stack;
    }

    public JTextField getInput() {
        return input;
    }

    
    
    private void configureTextField(JTextField field) {
        field.setEditable(false);
        field.setPreferredSize(new Dimension(30, 30));
    }

}
