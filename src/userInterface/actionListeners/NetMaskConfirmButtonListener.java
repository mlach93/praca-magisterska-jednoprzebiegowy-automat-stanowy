/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.actionListeners;

import applicationLogic.ipOperations.NetMask;
import applicationLogic.machine.ClassifierMachine;
import applicationLogic.machine.MachineStack;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JSlider;
import userInterface.ApplicationWindow;
import userInterface.panels.NetMaskPanel;

/**
 *
 * @author Mateusz
 */
public class NetMaskConfirmButtonListener implements ActionListener {

    private NetMaskPanel netMaskPanel;
    private MachineStack stack;
    private NetMask netMask;

    public NetMaskConfirmButtonListener(ApplicationWindow applicationWindow) {

        this.netMaskPanel = applicationWindow.getNetMaskPanel();
        ClassifierMachine classifierMachine = applicationWindow.getClassifierMachine();

        this.stack = classifierMachine.getStack();
        this.netMask = classifierMachine.getNetMask();

        
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        updateNetMask();
        disableNetMaskPanel();
        updateStack();

    }

    private void disableNetMaskPanel() {
        netMaskPanel.getNetMaskSlider().setEnabled(false);
        netMaskPanel.getNetMaskValue().setEnabled(false);
        netMaskPanel.getConfirmButton().setEnabled(false);
    }

    private void updateStack() {
        stack.resetStack();
        //if(netMask.getCidrState() ) Tutaj sprawdz czy zostala wybrana CIDR i jesli tak to czy pierwsze trzy bity adresu zaczynaja sie od 111
        stack.pushNext(netMask.getNetMask());
    }

    private void updateNetMask() {
        JSlider netMaskSlider = netMaskPanel.getNetMaskSlider();
        netMask.updateMask(netMaskSlider.getValue());
    }

}
