/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.actionListeners;

import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import userInterface.ApplicationWindow;
import userInterface.panels.NetMaskPanel;

/**
 *
 * @author Mateusz
 */
public class NetMaskSliderListener implements ChangeListener {

    private NetMaskPanel netMaskPanel;

    public NetMaskSliderListener(ApplicationWindow applicationWindow) {

        this.netMaskPanel = applicationWindow.getNetMaskPanel();
    }

    @Override
    public void stateChanged(ChangeEvent ce) {
        
        JSlider netMaskSlider = netMaskPanel.getNetMaskSlider();
        JTextField netMaskValue = netMaskPanel.getNetMaskValue();

        netMaskValue.setText(Integer.toString(netMaskSlider.getValue()));       //sets number of mask bits in box next to slider

    }

}
