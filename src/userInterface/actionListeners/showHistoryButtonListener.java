/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.actionListeners;

import applicationLogic.machine.ClassifierMachine;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import userInterface.ApplicationWindow;

/**
 *
 * @author Mateusz
 */
public class showHistoryButtonListener implements ActionListener {

    private ClassifierMachine classifierMachine;

    public showHistoryButtonListener(ApplicationWindow applicationWindow) {
        this.classifierMachine = applicationWindow.getClassifierMachine();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String history = buildHistory();
        JOptionPane.showMessageDialog(null, history);
    }

    private String buildHistory() {
        String history = classifierMachine.getMachineMemory().getMemory();

        if (history.isEmpty()) {
            history = "Żadne kroki nie zostaly jeszcze zapisane";
        } else {
            history += "\n";
        }
        return history;
    }

}
