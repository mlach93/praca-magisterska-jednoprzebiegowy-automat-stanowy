/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.actionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import applicationLogic.machine.ClassifierMachine;
import applicationLogic.machine.helpers.MachineMemory;
import java.util.EmptyStackException;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import userInterface.ApplicationWindow;
import userInterface.panels.MachineStatePanel;

/**
 *
 * @author Mateusz
 */
public class AutomaticStartButtonListener implements ActionListener {

    private ClassifierMachine classifierMachine;
    private JTextField step;
    private JTextField state;
    private JTextField stack;
    private JTextField input;

    public AutomaticStartButtonListener(ApplicationWindow applicationwindow) {

        this.classifierMachine = applicationwindow.getClassifierMachine();
        MachineStatePanel statePanel = applicationwindow.getStatePanel();

        this.step = statePanel.getStep();
        this.state = statePanel.getState();
        this.input = statePanel.getInput();
        this.stack = statePanel.getStack();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        try {
            step.setText(classifierMachine.getStep());
            state.setText(classifierMachine.getState());
            input.setText(classifierMachine.getInput());
            classifierMachine.automaticStart(this);
            stack.setText(classifierMachine.getTopOfStack());

        } catch (NullPointerException e) {

            //System.err.println("Address IP is not confirmed");
            step.setText("");
            state.setText("");
            input.setText("");
            stack.setText("");

            //e.printStackTrace(System.out);
            JOptionPane.showMessageDialog(null, "Address IP jest nie zatwierdzony. Aby kontynuować klinjij przycisk \"Potwierdź\"");

        } catch (StringIndexOutOfBoundsException e) {

            System.err.println("Machine ended up work");
            JOptionPane.showMessageDialog(null, "Automat zakończył swoją pracę. Aby rozpocząć od nowa ustaw automat w stan początkowy");

        } catch (EmptyStackException e) {

            //System.err.println("Machine ended up work");
            //e.printStackTrace(System.out);
            MachineMemory machineMemory = classifierMachine.getMachineMemory();
            
            step.setText(machineMemory.getLastStep());
            state.setText(machineMemory.getLastState());
            JOptionPane.showMessageDialog(null, "Aby kontynuowac zatwierdz maske sieciową");

        }

    }

    public void presentInWindow() {

        step.setText(classifierMachine.getStep());
        state.setText(classifierMachine.getState());
        input.setText(classifierMachine.getInput());
        stack.setText(classifierMachine.getTopOfStack());

    }

}
