package userInterface;

import userInterface.panels.*;
import userInterface.actionListeners.*;
import applicationLogic.machine.ClassifierMachine;
import applicationLogic.ipOperations.*;
import java.awt.*;
import javax.swing.*;
import static javax.swing.WindowConstants.*;
import userInterface.actionListeners.DiagramButtonListener;


/**
 *
 * @author Mateusz
 */
public class ApplicationWindow implements Runnable {


    private AddressIP addressIP;
    private ClassifierMachine classifierMachine;
    private MachineStatePanel statePanel;
    private IPPanel ipConfirmPanel;
    private NetMaskPanel netMaskPanel;
    private SummaryPanel summaryPanel;

    private void initializeComponents() {  // sets all starting values
        this.addressIP = new AddressIP();
        this.classifierMachine = new ClassifierMachine(this.addressIP, this);
        this.statePanel = createMachineStatePanel();
        this.ipConfirmPanel = new IPPanel();
        this.netMaskPanel = new NetMaskPanel();
        this.summaryPanel = new SummaryPanel();
    }

    public AddressIP getAddressIP() {
        return addressIP;
    }

    public ClassifierMachine getClassifierMachine() {
        return classifierMachine;
    }

    public MachineStatePanel getStatePanel() {
        return statePanel;
    }

    public IPPanel getIpConfirmPanel() {
        return ipConfirmPanel;
    }

    public NetMaskPanel getNetMaskPanel() {
        return netMaskPanel;
    }

    public SummaryPanel getSummaryPanel() {
        return summaryPanel;
    }

    @Override
    public void run() {
        initializeComponents();

        JFrame window = new JFrame("Klasyfikator adresów IP");
        window.setDefaultCloseOperation(EXIT_ON_CLOSE);
        window.setPreferredSize(new Dimension(800, 400));

        addComponents(window.getContentPane());

        window.pack();
        window.setVisible(true);

    }

    private void addComponents(Container container) {
        /* Method used to add components to window*/

        container.setLayout(new GridLayout(0, 1));
        container.add(createIpConfirmPanel());
        container.add(createNetMaskPanel());
        container.add(getStatePanel());
        container.add(createControlPanel());
        container.add(createSummaryPanel());

    }

    private JPanel createIpConfirmPanel() { //Panel on UI which is used to provide and confirm address IP

        JButton confirmButton = ipConfirmPanel.getConfirmButton();
        JCheckBox cidrCheckBox = ipConfirmPanel.getCidrCheckBox();

        confirmButton.addActionListener(new IPConfirmButtonListener(this));
        cidrCheckBox.addChangeListener(new CIDRListener(this));

        return ipConfirmPanel;
    }

    private JPanel createNetMaskPanel() {

        JSlider netMaskSlider = netMaskPanel.getNetMaskSlider();
        JButton confirmButton = netMaskPanel.getConfirmButton();
        JTextField netMaskValue = netMaskPanel.getNetMaskValue();

        netMaskSlider.addChangeListener(new NetMaskSliderListener(this));
        confirmButton.addActionListener(new NetMaskConfirmButtonListener(this));

        Integer value = netMaskSlider.getValue();
        netMaskValue.setText(value.toString());

        netMaskPanel.setVisible(false);
        return netMaskPanel;
    }

    private JPanel createControlPanel() {
        //this method adds pannel with automaticStart button

        ControlPanel controlPanel = new ControlPanel();

        JButton automaticStartButton = controlPanel.getAllStepsButton();
        JButton nextStepButton = controlPanel.getNextStepButton();
        JButton resetButton = controlPanel.getResetButton();
        JButton showHistoryButton = controlPanel.getShowHistoryButton();
        JButton diagramButton = controlPanel.getDiagramButton();

        automaticStartButton.addActionListener(new AutomaticStartButtonListener(this));
        nextStepButton.addActionListener(new NextStepButtonListener(this));
        resetButton.addActionListener(new ResetButtonListener(this));
        showHistoryButton.addActionListener(new showHistoryButtonListener(this));
        diagramButton.addActionListener(new DiagramButtonListener());
                

        return controlPanel;

    }

    private MachineStatePanel createMachineStatePanel() {

        MachineStatePanel machineStatePanel = new MachineStatePanel();
        
        return machineStatePanel;
    }

    private JPanel createSummaryPanel() {

        return summaryPanel;
    }



}
